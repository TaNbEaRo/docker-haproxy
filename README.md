Preconditions
1. java installed on your computer
2. maven installed on your computer
3. docker installed on your computer

1. open a terminal from the root directory of the project
2. execute "start-application"
3. try to send get http requests to localhost:9000 and localhost:9000/count 

clean up
1. open a terminal from the root directory of the project
2. execute "cleanup"

**info:**
Container port 9000 and 9001:
In an ideal case this port shifting is not necessary,
because both server runs in different containers which means they have different hosts.
But HA proxy checks address in parse time, so it fails to start if we specify a container id or name as a host address.
So that we had to give localhost (or other valid host address) as hostname for both of our server containers.
because of this we must shift the ports so they can run on the same host.

Expose doesn't necessary in dockerfile: 
The EXPOSE instruction does not actually publish the port.
It functions as a type of documentation between the person who builds the image and the person who runs the container, about which ports are intended to be published. 
To actually publish the port when running the container, use the -p flag on docker run to publish and map one or more ports, or the -P flag to publish all exposed ports and map them to high-order ports.

Get logs for exited container in case of any error:
execute in terminal "docker logs -t docker-proxy-poc-haproxy-container"
