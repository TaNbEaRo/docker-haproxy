package com.thecoffee.dockerpoc.restresources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class InsideService {

	private final ConfigurationService configurationService;

	public static AtomicInteger counter = new AtomicInteger(0);

	@Autowired
	public InsideService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	@Scheduled(fixedDelay = 500)
	public void getInside() {
		String inside = configurationService.restTemplate().getForObject(configurationService.insideUrl.concat("/inside"), String.class);
		if (inside != null) {
			counter.incrementAndGet();
		}
	}

	public int getCount(){
		return counter.get();
	}
}
