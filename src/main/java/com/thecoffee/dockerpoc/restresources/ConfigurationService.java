package com.thecoffee.dockerpoc.restresources;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigurationService {

	@Value("${inside-url}")
	public String insideUrl;

	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
