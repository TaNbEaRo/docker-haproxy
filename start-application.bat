call mvn package
call docker network create --driver bridge docker-proxy-poc || true
call docker build --network docker-proxy-poc -t docker-proxy-poc-server-1 --build-arg JAVA_OPTS="server.port=9000 inside-url=http://localhost:9001/api/one" ./docker
call docker build --network docker-proxy-poc -t docker-proxy-poc-server-2 --build-arg JAVA_OPTS="server.port=9001 inside-url=http://localhost:9000/api/two" ./docker
call docker build --network docker-proxy-poc -t docker-proxy-poc-haproxy -f ./docker/haproxy ./docker
call docker run -d --name docker-proxy-poc-server-1-container --hostname localhost docker-proxy-poc-server-1
call docker run -d --name docker-proxy-poc-server-2-container --hostname localhost docker-proxy-poc-server-2
call docker run -d --name docker-proxy-poc-haproxy-container -p 9000:9000 docker-proxy-poc-haproxy
